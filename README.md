CSCI4140 (2013 Spring) Open source software development course project of The Chinese University of Hong Kong

This is a course project about a turn-based web game that requires Player to use simple programming to command his character to clear levels.   

use javascript with node-browserify, CreateJS (http://www.createjs.com/ by Adobe)   
Most assets are from Ragnarok Battle Offline (http://en.wikipedia.org/wiki/Ragnarok_Battle_Offline)    

Project Name : Code Defense

Requirement
* Node.js


Update Node.js Dependences   
npm install  

Run Server  
node app -dev  

Build client-side Javascript        
node build   
Build client-side Javascript (development)      
node build -dev